PROTO_DIR = api/grpc/v1
OUTPUT_DIR = ./generate_grpc

.PHONY: generate
generate: create generate_proto move clean

.PHONY: generate_proto
generate_proto:
	protoc \
		--proto_path=$(PROTO_DIR) \
		--go_out=$(OUTPUT_DIR) \
		--go-grpc_out=$(OUTPUT_DIR) \
		--go_opt=paths=source_relative \
		--go-grpc_opt=paths=source_relative \
		$(PROTO_DIR)/api.proto

.PHONY: clean
clean:
	rm -rf $(OUTPUT_DIR)

.PHONY: move
move:
	mv $(OUTPUT_DIR)/* $(PROTO_DIR)/

.PHONY: create
create:
	mkdir $(OUTPUT_DIR)