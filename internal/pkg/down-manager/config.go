package down_manager

import (
	"io"
	"os"
	"syscall"
	"time"
)

type Config struct {
	LoggerWriter io.Writer
	DownTimeout  time.Duration
	DownSignals  []os.Signal
}

func NewConfigDefault() Config {
	return Config{
		LoggerWriter: os.Stdout,
		DownTimeout:  time.Second * 10,
		DownSignals:  []os.Signal{syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT},
	}
}
