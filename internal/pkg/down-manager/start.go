package down_manager

import (
	"context"
	"os"
	"os/signal"
)

// Start Запустить менеджера и ожидать сигнала завершения
func (m *appManager) Start(ctx context.Context) {
	if ctx == nil {
		ctx = context.Background()
	}

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, m.downSignals...)

	m.print("app started")

	select {
	case <-sig:
		m.down()
	// Если контекст завершится, то необходимо запустить остановку.
	// Пример, завершение приложения по таймауту
	case <-ctx.Done():
		m.down()
	}
}
