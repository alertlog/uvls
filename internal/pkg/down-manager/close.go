package down_manager

import "fmt"

func (m *appManager) closeHandlers(ch chan struct{}) {
	for _, callback := range m.closeCallbacks {
		if err := callback(); err != nil {
			m.print(fmt.Sprintf("fail close hander: %s", err.Error()))
		}
	}
	ch <- struct{}{}
}
