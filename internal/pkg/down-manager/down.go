package down_manager

import (
	"context"
	"fmt"
)

// down Завершение приложения.
// Если не удается за отведенно время завершить все обработчики, то приложение завершится по таймауту
func (m *appManager) down() {
	m.print("app init down")
	timeoutCtx, cancel := context.WithTimeout(context.Background(), m.downTimeout)
	defer cancel()

	downCh := make(chan struct{})
	go m.closeHandlers(downCh)

	select {
	case <-timeoutCtx.Done():
		cancel()
		m.print(fmt.Sprintf("app shutdown: %s", timeoutCtx.Err().Error()))
	case <-downCh:
		m.print("app stopped")
	}

}
