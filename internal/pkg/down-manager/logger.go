package down_manager

import (
	"log"
	"os"
)

func NewLoggerDefault() log.Logger {
	return *log.New(os.Stdout, "App Manager", log.LstdFlags)
}

func (m *appManager) print(params ...any) {
	if !m.isNilLogger {
		m.logger.Print(params)
	}
}
