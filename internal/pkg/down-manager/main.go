// Package down_manager Менеджер корректного завершения работы приложения
package down_manager

import (
	"context"
	"log"
	"os"
	"time"
)

type appManager struct {
	logger         log.Logger     // логгер для записи событий
	isNilLogger    bool           // флаг, если передан io.Writer == nil, то логи не будут писаться
	downTimeout    time.Duration  // таймаут, для экстренного завершения работы менеджера, если какой-либо ресурс будет слишком долго завершаться
	downSignals    []os.Signal    // список сингалов, которые будут обработаны как сигналы, завершающие работу приложения
	closeCallbacks []func() error // список функций, которые необходимо вызвать при завершении работы приложения
}

type ApplicationManager interface {
	// Start Запустить менеджера и ожидать сигнала завершения
	Start(ctx context.Context)
	// AddCloseHandler Добавить обратный вызов в список, который будет использоваться при завершении работы
	AddCloseHandler(callback func() error)
}

// New Создать и вернуть экземпляр менеджера
func New(params *Config) ApplicationManager {
	result := appManager{}
	defaultConfig := NewConfigDefault()

	// Если не переданы параметры, то необходимо использовать базовые настройки
	if params == nil {
		result.logger = *log.New(defaultConfig.LoggerWriter, "", log.LstdFlags)
		result.isNilLogger = false
		result.downTimeout = defaultConfig.DownTimeout
		return &result
	}
	// Если передается пустой writer, то устанавливаем флаг
	if params.LoggerWriter == nil {
		result.isNilLogger = true
	} else {
		result.isNilLogger = false
		result.logger = *log.New(params.LoggerWriter, "", log.LstdFlags)
	}

	if params.DownTimeout == 0 {
		result.downTimeout = defaultConfig.DownTimeout
	}

	if params.DownSignals == nil {
		result.downSignals = defaultConfig.DownSignals
	} else {
		result.downSignals = params.DownSignals
	}

	return &result
}
