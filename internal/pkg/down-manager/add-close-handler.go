package down_manager

// AddCloseHandler Добавить обратный вызов в список, который будет использоваться при завершении работы
func (m *appManager) AddCloseHandler(callback func() error) {
	m.closeCallbacks = append(m.closeCallbacks, callback)
}
