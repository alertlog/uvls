package app

import (
	downManager "uvls/internal/pkg/down-manager"
)

func StartApplication() {
	manager := downManager.New(nil)
	manager.AddCloseHandler(infinityClose)
	manager.Start(nil)
}

func infinityClose() error {
	select {}
}
